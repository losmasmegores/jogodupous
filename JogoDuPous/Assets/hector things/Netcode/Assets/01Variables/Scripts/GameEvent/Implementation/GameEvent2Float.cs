using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "GameEvent", menuName = "GameEvent/GameEvent - string")]
public class GameEventString : GameEvent<string> { }