using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;

namespace m17
{
    public class GeroColorController : NetworkBehaviour
    {
        [Rpc(SendTo.ClientsAndHost)]
        public void ColorChangeRpc(Color color)
        {
            GetComponent<SpriteRenderer>().color = color;
        }
    }
}