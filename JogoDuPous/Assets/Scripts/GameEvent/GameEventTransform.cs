using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameEvent", menuName = "GameEvent/GameEventTransform")]
public class GameEventTransform : ScriptableObject
{
    private readonly List<GameEventListenerTransform> eventListeners =
        new List<GameEventListenerTransform>();

    public void Raise(Transform a)
    {
        for (int i = eventListeners.Count - 1; i >= 0; i--)
            eventListeners[i].OnEventRaised(a);
    }

    public void RegisterListener(GameEventListenerTransform listener)
    {
        if (!eventListeners.Contains(listener))
            eventListeners.Add(listener);
    }

    public void UnregisterListener(GameEventListenerTransform listener)
    {
        if (eventListeners.Contains(listener))
            eventListeners.Remove(listener);
    }
}