using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;
using Random = UnityEngine.Random;

public class JugadorCambiaSegunPantalla : NetworkBehaviour
{
    [SerializeField] private GameEventTransform jugadorPartida;
    public override void OnNetworkSpawn()
    {
        NetworkManager.Singleton.SceneManager.OnSceneEvent += CargaScena;
        base.OnNetworkSpawn();
    }
    public override void OnNetworkDespawn()
    {
        NetworkManager.Singleton.SceneManager.OnSceneEvent -= CargaScena;
        base.OnNetworkDespawn();
    }

    public void CargaScena(SceneEvent se)
    {
        String searchName = "";
        
        if (se.SceneName == "Partida")
        {
            searchName = "PlayerPartida";
        }
        Debug.Log(se.SceneName);
        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).name == searchName)
            {
                transform.GetChild(i).gameObject.SetActive(true);
                transform.position = new Vector3(Random.Range(-6f,-7.1f), 0.7f, 0);
                jugadorPartida.Raise(transform);
            }
            else
                transform.GetChild(i).gameObject.SetActive(false);
        }

    }

}
