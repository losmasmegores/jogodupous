using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameConf", menuName = "Scriptable Objects/Configuracion Partida")]
[Serializable]
public class ConfiguracionPartida : ScriptableObject
{
    [SerializeField] private int nPlayers;
    [SerializeField] private int nPozos;
    public int NPlayers
    {
        get => nPlayers;
        set => nPlayers = value;
    }
    
    public int NPozos
    {
        get => nPozos;
        set => nPozos = value;
    }
}
