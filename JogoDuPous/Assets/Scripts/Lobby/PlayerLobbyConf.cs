using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.UI;

public class PlayerLobbyConf : NetworkBehaviour
{
    [SerializeField] private PlayerPartida player;
    [SerializeField] private TMP_InputField inputname;
    [SerializeField] private Button colorButtonleft;
    [SerializeField] private Button colorButtonright;
    [SerializeField] private Color[] colors;
    [SerializeField] private Image colorImage;
    private int coloractual = 0;
    private string nombreactual;
    // Start is called before the first frame update
    public override void OnNetworkSpawn()
    {
        // base.OnNetworkSpawn();
        Debug.Log("Creando user "+OwnerClientId);
        if (!IsOwner)
        {
            colorButtonleft.gameObject.SetActive(false);
            colorButtonright.gameObject.SetActive(false);
            return;
        }
    }

    // public void changeName(string name)
    // {
    //     nombreactual = name;
    //     player.nombre = nombreactual;
    //     changeInputValueRpc();
    // }
    
    [Rpc(SendTo.Everyone)]
    private void changeInputValueRpc()
    {
       inputname.text  = nombreactual;
    }
    [Rpc(SendTo.Everyone)]
    public void sumaColorRpc()
    {
        coloractual++;
        if (coloractual >= colors.Length)
        {
            coloractual = 0;
        }
        changePlayerColorRpc();
        
    }
    [Rpc(SendTo.Everyone)]
    public void restaColorRpc()
    {
        coloractual--;
        if (coloractual < 0)
        {
            coloractual = colors.Length - 1;
        }

        changePlayerColorRpc();
    }
    [Rpc(SendTo.Everyone)]
    private void changeColorRpc()
    {
        colorImage.color = colors[coloractual];
    }

    [Rpc(SendTo.Server)]
    public void changePlayerColorRpc()
    {
        if(!IsServer) return;
        player.colorPlayer = colors[coloractual];
        changeColorRpc();
    }
}
