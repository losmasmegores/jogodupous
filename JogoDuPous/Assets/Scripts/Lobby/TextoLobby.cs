using System;
using TMPro;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.UIElements;
using Slider = UnityEngine.UI.Slider;

public class TextoLobby : NetworkBehaviour
{
    [SerializeField] private Slider slider;
    private TextMeshProUGUI texto;
    private NetworkVariable<float> value = new NetworkVariable<float>(0);
    private void Awake()
    {
        texto = GetComponent<TextMeshProUGUI>();
    }

    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();
        value.OnValueChanged += changeSliderPlayerRpc;
    }

    public void cambioJugadores()
    {
        value.Value = slider.value;
        //changeSliderPlayerRpc();
    }

    public void cambioPozos()
    {
        value.Value = slider.value;
        //changeSliderPozoRpc();
        NetworkManager.Singleton.gameObject.GetComponent<PartidaConfiguracion>().setPozosRPC((int)slider.value);
        
    }

    public void changeSliderPlayerRpc(float oldValue, float newValue)
    {
        slider.value = value.Value;
        texto.text = "Nº Jugadores: " + slider.value;
    }
    
    [Rpc(SendTo.Everyone)]
    public void changeSliderPozoRpc()
    {
        slider.value = value.Value;
        if(slider.value < 1)
            texto.text = "Nº Pozos: Pozo x Player";
        else
        {
            texto.text = "Nº Pozos: " + slider.value;
        }
    }
}
