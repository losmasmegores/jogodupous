using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JugadorLlega : MonoBehaviour
{
    [SerializeField] private GameEventTransform llegaJugador;

    [SerializeField] private Transform parentJugadores;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void llegaraJugador()
    {
        Debug.Log("Llega Jugador");
        llegaJugador.Raise(parentJugadores);
    }
}
