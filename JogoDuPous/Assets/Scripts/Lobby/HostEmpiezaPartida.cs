using Unity.Netcode;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;
using Slider = UnityEngine.UI.Slider;

public class HostEmpiezaPartida : MonoBehaviour
{
    [SerializeField] private Transform n_players;

    [SerializeField] private GameEvent startGame; 
    // [SerializeField] private NetworkSceneManager ns;
    [SerializeField] private PartidaConfiguracion cp;
    [SerializeField] private Slider slider;
    
    public void EmpiezaPartida()
    { 
        if (n_players.childCount > 1)
        {
            Debug.Log("Empieza la partida");
            // startGame.Raise();
        
            cp.NPlayers = n_players.childCount;
            if(slider.value > 0)
                cp.setPozosRPC((int)slider.value);
            else
                cp.setPozosRPC(n_players.childCount);
            
            
            NetworkManager.Singleton.SceneManager.LoadScene("Partida", LoadSceneMode.Single);
        }
        else
        {
            Debug.Log("No hay suficientes jugadores");
        }
    }
}
