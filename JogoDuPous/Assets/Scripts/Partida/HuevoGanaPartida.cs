using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HuevoGanaPartida : NetworkBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        if(!IsServer) return;
        NetworkManager.Singleton.SceneManager.LoadScene("FinalPartida", LoadSceneMode.Single);
    }
}
