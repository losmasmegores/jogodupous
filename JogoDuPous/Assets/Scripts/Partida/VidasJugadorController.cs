using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VidasJugadorController : MonoBehaviour
{
    [SerializeField] private GamePool pool;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetVidas(int vidas)
    {
        pool.ReturnAllElements();
        for (int i = 0; i < vidas; i++)
        {
            GameObject vida = pool.GetElement();
            vida.SetActive(true);
        }
    }
}
