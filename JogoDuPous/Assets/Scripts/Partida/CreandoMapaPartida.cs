using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class CreandoMapaPartida : NetworkBehaviour
{
    [SerializeField] private PartidaConfiguracion cp;
    [SerializeField] private float generaMapa;
    [SerializeField] private GameObject caminoPozo;
    [SerializeField] private GameObject caminoVictoria;
    [SerializeField] private Transform PozoParent;
    
    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();
        if(!IsServer) return;
        
        cp = NetworkManager.Singleton.gameObject.GetComponent<PartidaConfiguracion>();
        Debug.Log("POZOS:" +cp.getPozos());
        float position = 0;
        // PozoParent.GetComponent<NetworkObject>().Spawn();
        for (int i = 0; i < cp.getPozos(); i++)
        {
            GameObject camino = Instantiate(caminoPozo,PozoParent);
            // camino.transform.SetParent(PozoParent);
            // Debug.Log("Padre (antes de darle posicion): " + camino.transform.parent.name);
            camino.transform.position = new Vector3(position, -4.32f, 10);
            // camino.transform.SetParent(PozoParent);
            // Debug.Log("Padre (antes de spawnearlo): " + camino.transform.parent.name);
            camino.GetComponent<NetworkObject>().Spawn();
            // camino.GetComponent<NetworkObject>().TrySetParent(PozoParent);
            // Debug.Log("Padre (despues de spawnearlo): " + camino.transform.parent.name);
            position += generaMapa;
        }
        GameObject victoria = Instantiate(caminoVictoria,PozoParent);
        victoria.transform.position = new Vector3(position, -4.32f, 10);
        victoria.GetComponent<NetworkObject>().Spawn();
        gameObject.GetComponent<TurnosManager>().ComienzaPartida();
    }
    
    
}
