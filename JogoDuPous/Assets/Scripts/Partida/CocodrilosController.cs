using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class CocodrilosController : NetworkBehaviour
{
    [SerializeField] private GameObject cocodrilo;
    [SerializeField] private Transform agua;
    
    [Rpc(SendTo.Everyone)]
    public void AddCocodriloRpc()
    {
        if(transform.childCount < 1)agua.gameObject.SetActive(true);
        Instantiate(cocodrilo, transform);
    }
    public bool removeCocodrilo()
    {
        if (transform.childCount < 1) return false;
        removeCocodriloRpc();
        return true;
    }
    public int NumeroCocodrilos()
    {
        return transform.childCount;
    }

    [Rpc(SendTo.Everyone)]
    public void removeCocodriloRpc()
    {
        Destroy(transform.GetChild(0).gameObject);
        Debug.Log(transform.childCount); 
        if(transform.childCount < 1)agua.gameObject.SetActive(false);
    }
}
