using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiPaartidaPlayer : MonoBehaviour
{
    [SerializeField] private Transform Turno;
    [SerializeField] private Transform NoTurno;
    [SerializeField] private Transform NoHacerNada;
    
    public void MiTurno()
    {
        Turno.gameObject.SetActive(true);
        NoTurno.gameObject.SetActive(false);
        NoHacerNada.gameObject.SetActive(false);
    }
    public void NoMiTurno()
    {
        Turno.gameObject.SetActive(false);
        NoTurno.gameObject.SetActive(true);
        NoHacerNada.gameObject.SetActive(false);
    }
    public void NoHagasNada()
    {
        NoHacerNada.gameObject.SetActive(true);
        NoTurno.gameObject.SetActive(false);
        Turno.gameObject.SetActive(false);
    }
}
