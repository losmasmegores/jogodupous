using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class CamaraDelJuego : NetworkBehaviour
{
    [SerializeField]private Transform players;
    private Transform target = null;
    
    [Rpc(SendTo.Everyone)]
    public void getActualPlayerRpc(int turno)
    { 
        target = players.GetChild(turno).GetChild(0);
    }
   void Update()
    {
        if (target != null)
        {
            transform.position = new Vector3(target.position.x, target.position.y, transform.position.z);
        }
    }
}
