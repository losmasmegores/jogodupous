using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEditor;
using UnityEngine;

public class DadoJuego : NetworkBehaviour
{
    private float posicionX = 0;
    private float posicionY = 0;
    // Update is called once per frame
    void Update()
    {
        // if(transform.eulerAngles.x < posicionX-10f)
        //     transform.Rotate(0.5f,0,0);
        // else if (transform.eulerAngles.x > posicionX + 10f)
        //     transform.Rotate(0.5f, 0, 0);
        // else if(transform.eulerAngles.x != posicionX) transform.localEulerAngles = new Vector3(posicionX, transform.eulerAngles.y, 0);
        // if(transform.eulerAngles.y < posicionY-10f)
        //     transform.Rotate(0,0.5f,0);
        // else if (transform.eulerAngles.y > posicionY + 10f)
        //     transform.Rotate(0, 0.5f, 0);
        // else if(transform.eulerAngles.y != posicionY) transform.localEulerAngles = new Vector3(transform.eulerAngles.x, posicionY, 0);
    }

    [Rpc(SendTo.Everyone)]
    public void setPosicionRpc(float x, float y)
    {
        posicionX = x;
        posicionY = y;
        transform.localEulerAngles = new Vector3(posicionX, posicionY, 0);
    }

    public void SetDadoSegunValor(int valor)
    {
        if(!IsServer) return;
        switch (valor)
        {
            case 1:
                setPosicionRpc(180, 0);
                break;
            case 2:
                setPosicionRpc(-90, 0);
                break;
            case 3:
                setPosicionRpc(0, -90);
                break;
            case 4:
                setPosicionRpc(0, 90);
                break;
            case 5:
                setPosicionRpc(90, 0);
                break;
            case 6:
                setPosicionRpc(0, 0);
                break;
        }
    }
}
