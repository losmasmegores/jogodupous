using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using Unity.Netcode;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;
using Random = System.Random;

public class TurnosManager : NetworkBehaviour
{
    // NetworkVariable<int> turnoActual = new NetworkVariable<int>();
    int turnoActual = Int32.MinValue;
    private bool finalPartida = false;
    [Header("Transforms")]
    [SerializeField] private Transform Players;
    [SerializeField] private Transform pozos;
    [Header("GameEvents")]
    [SerializeField] private GameEvent miturno;
    [SerializeField] private GameEvent nomiturno;
    [SerializeField] private GameEvent nohacernada;
    [SerializeField] private GameEventInt vidasJugador;
    [Header("Camera and Dice")]
    [SerializeField] private CamaraDelJuego camara;
    [SerializeField] private DadoJuego dado;
    private Transform ActualPlayer;
    public void ComienzaPartida()
    {
        if (!IsServer) return;
        StartCoroutine(StartGame());
    }

    public void Turno(int turno)
    {
        // Debug.Log("Turno: " + turno);
        // Debug.Log("TurnoActual: " + turnoActual);
        if (Players.childCount > 1)
        {
            // Transform jugador = Players.GetChild(turnoActual.Value);
            ActualPlayer = Players.GetChild(turnoActual);
            vidasJugador.Raise(ActualPlayer.gameObject.GetComponent<PlayerPartida>().health);
            if (ActualPlayer.gameObject.GetComponent<NetworkObject>().OwnerClientId == NetworkManager.Singleton.LocalClientId)
            {
                // Debug.Log("Es mi turno");
                miturno.Raise();
            }
            else
            {
                // Debug.Log("No es mi turno");
                nomiturno.Raise();
            }
            // if(!IsServer) return;
            
        }
        else
        {
            if (!IsServer) return;
            NetworkManager.Singleton.SceneManager.LoadScene("FinalPartida", LoadSceneMode.Single);
        }
        // else nomiturno.Raise();
    }

    [Rpc(SendTo.Server)]
    public void SaltarRpc()
    {
        if(!IsServer) return;
        // setVidasRpc();
        camara.getActualPlayerRpc(turnoActual);
        PlayerPartida playerPartida = ActualPlayer.gameObject.GetComponent<PlayerPartida>();
        
        Debug.Log("Salto");
        int numeroMinimoParaPoderSaltar = 5;
        if (playerPartida.EstaCorriendo())
        {
            numeroMinimoParaPoderSaltar = 4;
            playerPartida.DejaDeCorrer();
        }
        int numero = new Random().Next(0, 6)+1;
        dado.SetDadoSegunValor(numero);
        Debug.Log("El dado ha sacado un "+ numero);
        if (numero >= numeroMinimoParaPoderSaltar)
        {
            playerPartida.salePozo();
            Transform pozoSiguiente = pozos.GetChild(playerPartida.PozoActual() + 1);
            
            // ActualPlayer.position = Vector3.Slerp(ActualPlayer.position, pozoSiguiente.GetChild(0).position+new Vector3(0,2,0), 1);
            setPositionRpc(pozoSiguiente.GetChild(0).position+new Vector3(0,2,0));
            playerPartida.PasaPozo();
            if(numero == 6)SiguienteTurnoRpc(turnoActual);
            else SiguienteTurnoRpc(turnoActual+1);
        }
        else
        {
            playerPartida.entraPozo();
            ComprobarSiHayCocodrilos(playerPartida);
            if (numero == 1)
            {
                playerPartida.RestarVida();
                setVidasRpc();
            }
            Transform pozo = pozos.GetChild(playerPartida.PozoActual());
            if (Vector3.Distance(ActualPlayer.position, pozo.GetChild(2).GetChild(0).position + new Vector3(0, 4, 0)) > 1f)
            {
                setPositionRpc(pozo.GetChild(2).GetChild(0).position+new Vector3(0,4,0));
            }
            // ActualPlayer.position = Vector3.Slerp(ActualPlayer.position, pozo.GetChild(2).GetChild(0).position+new Vector3(0,4,0), 1);
            SiguienteTurnoRpc(turnoActual+1);
        }
        // SiguienteTurnoRpc(turnoActual.Value++);
    }

    [Rpc(SendTo.Server)]
    public void CorrerRpc()
    {
        if(!IsServer) return;
        camara.getActualPlayerRpc(turnoActual);
        PlayerPartida playerPartida = ActualPlayer.gameObject.GetComponent<PlayerPartida>();
        if (playerPartida.EstaCorriendo())
        {
            Debug.Log("No puedes Correr si estas corriendo!!");
            SiguienteTurnoRpc(turnoActual);
        }
        else
        {
            Debug.Log("Intento Correr");
            int numero = new Random().Next(0, 6)+1;
            dado.SetDadoSegunValor(numero);
            Debug.Log("El dado ha sacado un "+ numero);
            if(playerPartida.EstaDentroDeUnPozo()) ComprobarSiHayCocodrilos(playerPartida);
            if (numero > 2)
            {
                playerPartida.Corriendo();
                if(numero == 6)SiguienteTurnoRpc(turnoActual);
                else SiguienteTurnoRpc(turnoActual+1);
            }
            else if (numero == 1)
            {
                CocodrilosController pozo = pozos.GetChild(playerPartida.PozoActual()).GetChild(2).GetChild(2).GetComponent<CocodrilosController>();
                pozo.AddCocodriloRpc();
                // playerPartida.RestarVida();
                SiguienteTurnoRpc(turnoActual+1);
            }
            else SiguienteTurnoRpc(turnoActual+1);   
        }
    }

    public void NoHacerNada()
    {
        // NoHacerNadaRpc(RpcTarget.Single(ActualPlayer.gameObject.GetComponent<NetworkObject>().OwnerClientId,RpcTargetUse.Temp));
        NoHacerNadaRpc();
        
        // if (ActualPlayer.gameObject.GetComponent<NetworkObject>().OwnerClientId == NetworkManager.Singleton.LocalClientId)
        // {
        //     nohacernada.Raise();
        //     camara.getActualPlayerRpc(turnoActual);
        // }
        // SiguienteTurnoRpc(turnoActual.Value++);
        // Debug.Log("No hago nada");
    }
    // [Rpc(SendTo.SpecifiedInParams)]
    [Rpc(SendTo.Everyone)]
    public void NoHacerNadaRpc()
    {
        // nohacernada.Raise();
        // 
        if (ActualPlayer.gameObject.GetComponent<NetworkObject>().OwnerClientId == NetworkManager.Singleton.LocalClientId)
        {
            nohacernada.Raise();
            if (!IsServer) return;
            camara.getActualPlayerRpc(turnoActual);
        }
    }

    [Rpc(SendTo.Everyone)]
    public void SiguienteTurnoRpc(int value)
    {
        if(ActualPlayer != null)setVidasRpc();
        if(value > Players.childCount-1)
        {
            value = 0;
        }
        // if(IsServer) turnoActual.Value = value;
        turnoActual = value;
        Turno(turnoActual);
        // Turno(turnoActual.Value);
    }
    
    public IEnumerator StartGame()
    {
        Debug.Log("Corrutina Empezada");
        while(Players.childCount < NetworkManager.Singleton.ConnectedClientsList.Count)
        {
            Debug.Log("Numero de jugadores actualmente: "+Players.childCount);
            yield return new WaitForSeconds(1);
        }
        Debug.Log("Empieza Partida");
        for (int i = 0; i < Players.childCount; i++)
        {
            PlayerPartida p = Players.GetChild(i).GetComponent<PlayerPartida>();
            p.onDieEvent += PlayerMuerto;
        }
        SiguienteTurnoRpc(0);
        // Turno(turnoActual);
    }

    [Rpc(SendTo.Everyone)]
    public void setPositionRpc(Vector3 pozoalquellegar)
    {
        // float actualposition = 0;
        // while (actualposition < 1)
        // {
        //     player.position = Vector3.Slerp(player.position, pozoalquellegar, actualposition);
        //     actualposition += 0.1f;
        //     yield return new WaitForSeconds(0.2f);
        // }
        
        float duracion = 3f;
        float v0_x = (pozoalquellegar.x - ActualPlayer.GetChild(0).transform.position.x) / (duracion);
        
        // Aislar v0 en la segunda ecuación
        float v0_y = ((pozoalquellegar.y - ActualPlayer.GetChild(0).transform.position.y) + (0.5f * 9.8f * duracion * duracion)) / (duracion);
        
        ActualPlayer.GetChild(0).GetComponent<Rigidbody2D>().velocity = new Vector2(v0_x, v0_y);
    }

    [Rpc(SendTo.Everyone)]
    public void setVidasRpc()
    {
        vidasJugador.Raise(ActualPlayer.gameObject.GetComponent<PlayerPartida>().health);
    }

    public void ComprobarSiHayCocodrilos(PlayerPartida playerPartida)
    {
        CocodrilosController pozo = pozos.GetChild(playerPartida.PozoActual()).GetChild(2).GetChild(2).GetComponent<CocodrilosController>();
        if (pozo.NumeroCocodrilos() > 0)
        {
            for (int i = 0; i < pozo.NumeroCocodrilos(); i++)
            {
                playerPartida.RestarVida();
            }
        }
    }

    [Rpc(SendTo.Server)]
    public void curarPlayerRpc()
    {
        if(!IsServer) return;
        PlayerPartida playerPartida = ActualPlayer.gameObject.GetComponent<PlayerPartida>();
        if(!playerPartida.curarme())Debug.Log("No puedes curarte mas");
        else SiguienteTurnoRpc(turnoActual+1);
    }

    [Rpc(SendTo.Server)]
    public void quitarCocodriloRpc()
    {
        if(!IsServer) return;
        CocodrilosController pozo = pozos.GetChild(ActualPlayer.gameObject.GetComponent<PlayerPartida>().PozoActual()).GetChild(2).GetChild(2).GetComponent<CocodrilosController>();
        if(!pozo.removeCocodrilo())Debug.Log("No hay cocodrilos en este pozo");
        else SiguienteTurnoRpc(turnoActual+1);
    }

    public void PlayerMuerto()
    {
        if(Players.childCount == 2)
        {
            finalPartida = true;
            NetworkManager.Singleton.SceneManager.LoadScene("FinalPartida", LoadSceneMode.Single);
        }
        else
        {
            SiguienteTurnoRpc(turnoActual);
        }
    }
}
