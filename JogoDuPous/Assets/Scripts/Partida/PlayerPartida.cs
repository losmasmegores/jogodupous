using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using Unity.VisualScripting;
using UnityEngine;

public class PlayerPartida : NetworkBehaviour
{
    
    // [SerializeField] private NetworkVariable<> name = new NetworkVariable<string>("?");
    [SerializeField] private NetworkVariable<Color> color = new NetworkVariable<Color>(Color.white);
    [SerializeField] private NetworkVariable<int> vidas = new NetworkVariable<int>(4);
    [SerializeField] private NetworkVariable<int> pozoActual = new NetworkVariable<int>(0);
    [SerializeField] private NetworkVariable<bool> corriendo = new NetworkVariable<bool>(false);
    [SerializeField] private NetworkVariable<bool> estaDentroDeUnPozo = new NetworkVariable<bool>(false);
    [SerializeField] private Transform particulas;
    public delegate void OnDie();

    public event OnDie onDieEvent;

    private void Awake()
    {
        // throw new NotImplementedException();
        color.OnValueChanged += OnColorChanged;
    }

    public override void OnNetworkDespawn()
    {
        // base.OnNetworkDespawn();
        onDieEvent.Invoke();
    }

    // public string nombre
    // {
    //     get => name.Value;
    //     set => name.Value = value;
    // }
    public Color colorPlayer
    {
        get => color.Value;
        set => color.Value = value;
    }
    public int health
    {
        get => vidas.Value;
        // set => vidas.Value = value;
    }
    public void RestarVida()
    {
        if (!IsServer) return;
        vidas.Value--;
        if (vidas.Value <= 0)
        {
            
            gameObject.GetComponent<NetworkObject>().Despawn(true);
        }
    }

    public void Corriendo()
    {
        if (!IsServer) return;
        corriendo.Value = true;
        setParticulasRpc(true);
    }
    public void DejaDeCorrer()
    {
        if (!IsServer) return;
        corriendo.Value = false;
        setParticulasRpc(false);
    }
    public bool EstaCorriendo()
    {
        return corriendo.Value;
    }
    public int PozoActual(){
        return pozoActual.Value;
    }

    public void PasaPozo()
    {
        pozoActual.Value++;
    }
    
    [Rpc(SendTo.Everyone)]
    public void setParticulasRpc(bool activar)
    {
        particulas.gameObject.SetActive(activar);
    }

    public void entraPozo()
    {
        estaDentroDeUnPozo.Value = true;
    }
    public void salePozo()
    {
        estaDentroDeUnPozo.Value = false;
    }
    public bool EstaDentroDeUnPozo()
    {
        return estaDentroDeUnPozo.Value;
    }

    public bool curarme()
    {
        if(health == 4) return false;
        else
        {
            vidas.Value++;
            return true;
        }
    }
    
    public void OnColorChanged(Color previousValue, Color newValue)
    {
        transform.GetChild(0).GetComponent<SpriteRenderer>().color = newValue;
        particulas.GetComponent<ParticleSystem>().startColor = newValue;
    }

}
