using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.UI;

public class PartidaConfiguracion : MonoBehaviour
{
    private NetworkVariable<int> NPozos = new NetworkVariable<int>(1);
    public int NPlayers { get; set; }
    public Slider sliderPozo;

    void Awake()
    {
        NPozos.OnValueChanged += (int pastValue,int newValue) =>
        {
            Debug.Log("NPozos: " + newValue);
            sliderPozo.value = newValue;
        };
        if (NetworkManager.Singleton.IsServer)
            NetworkManager.Singleton.OnConnectionEvent += (clientId, state) =>
            {
                if (NPozos.Value == NetworkManager.Singleton.ConnectedClients.Count) setPozosRPC(NPozos.Value++);
            };
    }

     [Rpc(SendTo.Everyone)]
    public void setPozosRPC(int newValue)
    {
        Debug.Log("NPozos: " + newValue);
        NPozos.Value = newValue;
        // sliderPozo.value = newValue;
    }
    public int getPozos()
    {
        return NPozos.Value;
    }

    public void PozoChangeValue()
    {
        setPozosRPC((int)sliderPozo.value);
    }
}
