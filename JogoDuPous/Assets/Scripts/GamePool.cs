using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class GamePool : MonoBehaviour
{
    private int size;

    private int activeMembers;

    // Start is called before the first frame update
    void Awake()
    {
        size = transform.childCount;
    }

    public GameObject GetElement()
    {
        GameObject go = null;
        //Debug.Log(size);
        for (int i = 0; i < size; i++)
        {
            go = transform.GetChild(i).gameObject;
            if (!go.activeSelf)
            {
                activeMembers++;
                return go;
            }
        }
        return null;
    }

    public void ReturnElement(GameObject go)
    {
        for (int i = 0; i < size; i++)
        {
            GameObject selfgo = transform.GetChild(i).gameObject;
            if (selfgo == go)
            {
                activeMembers--;
                go.SetActive(false);
                return;
            }
        }

        Assert.IsFalse(false);
    }

    public int getActiveMembers()
    {
        //int count = 0;
        //Debug.Log("este es mi tamaño: " + size);
        //for (int i = 0; i < size; i++)
        //{
        //    GameObject selfgo = transform.GetChild(i).gameObject;
        //    if (selfgo.activeSelf) count++;
        //}
        //return count;
        return activeMembers;
    }

    public void ReturnAllElements()
    {
        for (int i = 0; i < size; i++)
        {
            GameObject selfgo = transform.GetChild(i).gameObject;
            if (selfgo.activeSelf)
            {
                activeMembers--;
                selfgo.SetActive(false);
            }
        }
    }
}
