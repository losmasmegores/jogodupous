using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.Netcode;
using Unity.Netcode.Transports.UTP;
using UnityEngine;
using UnityEngine.UI;

namespace m17
{
    [GenerateSerializationForTypeAttribute(typeof(System.String))] 
    [GenerateSerializationForType(typeof(string))]
    public class HandleConnectionUI : MonoBehaviour
    {
        [SerializeField]
        private Button m_ServerButton;
        [SerializeField]
        private Button m_ClientButton;
        [SerializeField]
        private Button m_HostButton;
        [SerializeField] private TextMeshProUGUI m_IPText;
        

        void Awake()
        {
            // m_ServerButton.onClick.AddListener(() =>
            // {
            //     NetworkManager.Singleton.StartServer();
            // });

            m_ClientButton.onClick.AddListener(() =>
            {
                // if(m_IPText.text != " ")
                //     NetworkManager.Singleton.GetComponent<UnityTransport>().ConnectionData.Address = m_IPText.text;
                NetworkManager.Singleton.StartClient();
            });

            m_HostButton.onClick.AddListener(() =>
            {
                // if (m_IPText.text != string.Empty)
                // {
                //     Debug.Log(m_IPText.text);
                //     NetworkManager.Singleton.GetComponent<UnityTransport>().ConnectionData.Address = m_IPText.text;
                // }
                NetworkManager.Singleton.StartHost();
            });
        }

        public void setId(string text)
        {
            NetworkManager.Singleton.GetComponent<UnityTransport>().ConnectionData.Address = text;
        }
    }
}

