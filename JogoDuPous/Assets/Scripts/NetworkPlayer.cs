using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class NetworkPlayer : NetworkBehaviour
{
    [SerializeField] private GameEvent llegue;
    
    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();
        Debug.Log(transform.GetChild(0).name);
        if(transform.GetChild(0).name.Equals("PlayerLobby"))transform.GetChild(0).gameObject.SetActive(true);
        llegue.Raise();


        //Codi que només s'executarà al servidor
        if (IsServer)
        {
            // transform.position = new Vector3(Random.Range(-7f,7f), Random.Range(-5f, 5f), 0);
        }

        //Codi que s'executarà només si ets owner
        if (!IsOwner)
            return;

        // m_OnIDChangedEvent.Raise("ID: " + OwnerClientId);
    }
    public override void OnNetworkDespawn()
    {
        base.OnNetworkDespawn();
        
    }
    void Update()
    {
        //Aquest update només per a qui li pertany
        if (!IsOwner)
            return;
    }
}
